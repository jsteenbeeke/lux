/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.lux;

import com.jeroensteenbeeke.lux.internal.Failure;
import com.jeroensteenbeeke.lux.internal.Success;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Untyped Result that is meant to replace a boolean return value. Indicates if an action was successful, and if not:
 * why the action failed
 */
public sealed interface ActionResult extends Result<ActionResult,ActionResult> permits Success, Failure {
	/**
	 * @see Result#isOk()
	 */
	@Override
	boolean isOk();

	/**
	 * @see Result#getMessage()
	 */
	@Override
	String getMessage();


	/**
	 * Create a new successful ActionResult
	 * @return A TypedResult with status ok
	 */
	@NotNull
	static ActionResult ok() {
		return Success.INSTANCE;
	}

	/**
	 * Creates a new ActionResult in error state, with the given formatted error message
	 * @param message Either the error message (if {@code params.length == 0}), or a String format to compose the error message from
	 * @param params The parameters to the String formatter
	 * @return A ActionResult with status failure, containing the formatted error message
	 * @see java.lang.String#format(String,Object...)
	 */
	@NotNull
	static ActionResult error(@NotNull String message, @NotNull Object... params) {
		if (params.length == 0) {
			return new Failure(message);
		} else {
			return new Failure(String.format(message, params));
		}
	}

	/**
	 * Creates a new TypedResult based on the current ActionResult. If the current result is ok, then the given supplier
	 * is used to create a new object to use in the TypedResult. Otherwise, an error-state TypedResult with the same error
	 * as the current result is returned
	 * @param objectSupplier A Supplier that creates the result object if the current result is successful
	 * @param <T> The type of the result object
	 * @return A TypedResult containing the supplier's object, or {@code null} otherwise
	 */
	@NotNull
	<T> TypedResult<T> createIfOk(@NotNull Supplier<T> objectSupplier);

	@Override
	@NotNull
	<TR extends Throwable> ActionResult throwIfNotOk(@NotNull Function<String, TR> errorToException) throws TR;

	/**
	 * Chain the current action result. If the current result is OK, evaluate the given supplier. If not, return current error
	 * @param actionResultSupplier The supplier to evaluate
	 * @return A new ActionResult (or the current one if not OK)
	 */
	@NotNull ActionResult andThen(@NotNull Supplier<ActionResult> actionResultSupplier);

	/**
	 * Attempts to perform the action specified by the runnable
	 * @param runnable The action to perform
	 * @return An ActionResult indicating either success or the reason for failure
	 */
	@NotNull
	static ActionResult attempt(@NotNull FailableRunnable runnable) {
		try {
			runnable.run();
			return ok();
		} catch (Exception e) {
			return error(e.getMessage());
		}
	}

	/**
	 * Operation without input or output, that may throw an exception
	 */
	@FunctionalInterface
	interface FailableRunnable extends Serializable {
		/**
		 * Perform the operation
		 * @throws Exception If the operation fails
		 */
		void run() throws Exception;
	}
}
