package com.jeroensteenbeeke.lux;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Generic result type that can be used instead of either a boolean return value or an Optional. In many cases, when
 * an action fails, we not only want to indicate success or failure, but also the reason for failure. Exceptions can
 * also be used in this case, but checked exceptions are rather invasive tools, and unchecked exceptions often get
 * ignored until they occur. This interface (and its implementations) offer another solution
 *
 * @param <T>  The type of the result (for method chaining in the implementing type)
 * @param <ER> The type of object returned by the {@link Result#throwIfNotOk(Function)} method
 */
public interface Result<T extends Result<T,ER>, ER> {
	/**
	 * Indicates whether or not the result was successful
	 *
	 * @return {@code true} if the result was successful, {@code false} otherwise
	 */
	boolean isOk();

	/**
	 * The reason for failure
	 *
	 * @return A {@code String} containing the reason for failure if {@code isOk()} returns {@code false}, or {@code
	 * null otherwise}
	 */
	String getMessage();

	/**
	 * Exposes the current result as a stream, if successful
	 *
	 * @return A {@code Stream} containing 0 (when {@code isOk() == false}) or 1 (when {@code isOk() == true}) elements
	 */
	@SuppressWarnings("unchecked")
	@NotNull
	default Stream<T> allOk() {
		if (isOk()) {
			return Stream.of((T) this);
		}

		return Stream.empty();
	}

	/**
	 * Exposes the current result as a stream, if unsuccessful
	 *
	 * @return A {@code Stream} containing 0 (when {@code isOk() == true}) or 1 (when {@code isOk() == false}) elements
	 */
	@SuppressWarnings("unchecked")
	@NotNull
	default Stream<T> allNotOk() {
		if (isOk()) {
			return Stream.empty();
		}

		return Stream.of((T) this);
	}

	/**
	 * Optionally execute the given runnable
	 *
	 * @param runnable The runnable to execute if {@code isOk() == true}.
	 * @return The current Result
	 */
	@SuppressWarnings("unchecked")
	@NotNull
	default T ifOk(Runnable runnable) {
		if (isOk()) {
			runnable.run();
		}

		return (T) this;
	}

	/**
	 * Optionally apply the given consumer
	 *
	 * @param consumer The consumer to pass the error message to if {@code isOk() == false}.
	 * @return The current Result
	 */
	@SuppressWarnings("unchecked")
	@NotNull
	default T ifNotOk(Consumer<String> consumer) {
		if (!isOk()) {
			consumer.accept(getMessage());
		}

		return (T) this;
	}

	/**
	 * Optionally throw an exception if {@code isOk() == false}
	 *
	 * @param errorToException A {@code Function} to translate the error message to an exception
	 * @param <TR>             The type of exception to throw
	 * @return An object indicated by the second generic type of this class
	 * @throws TR The exception created by to {@code errorToException} function
	 */
	@NotNull
	<TR extends Throwable> ER throwIfNotOk(Function<String, TR> errorToException) throws TR;
}
