package com.jeroensteenbeeke.lux;

import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static com.jeroensteenbeeke.lux.Both.both;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class BothTest {
	@Test
	public void testMapBothNonNull() {
		assertThat(createTestCase("Foo", "Bar").map(
			l -> l,
			r -> r,
			(l, r) -> l + r,
			() -> "None"
		), equalTo("FooBar"));

	}

	@Test
	public void testMapBothNull() {
		assertThat(createTestCase(null, null).map(
			l -> l,
			r -> r,
			(l, r) -> l + r,
			() -> "None"
		), equalTo("None"));

	}

	@Test
	public void testMapLeftNull() {
		assertThat(createTestCase(null, "Bar").map(
			l -> l,
			r -> r,
			(l, r) -> l + r,
			() -> "None"
		), equalTo("Bar"));

	}

	@Test
	public void testMapRightNull() {
		assertThat(createTestCase("Foo", null).map(
			l -> l,
			r -> r,
			(l, r) -> l + r,
			() -> "None"
		), equalTo("Foo"));
	}

	@Test
	public void testAlternateCondition() {
		assertThat(createTestCase("Foo", "Bar")
					   .withCondition(i -> i.startsWith("F")).map(
				l -> l,
				r -> r,
				(l, r) -> l + r,
				() -> "None"
			), equalTo("Foo"));
	}

	@Test
	public void testFoldBothNonNull() {
		assertThat(createTestCase("Foo", "Bar").fold(
			"Test",
			(i, l) -> i + l,
			(i, r) -> i + r,
			(i, l, r) -> i + l + r,
			i -> i + "None"
		), equalTo("TestFooBar"));

	}

	@Test
	public void testFoldBothNull() {
		assertThat(createTestCase(null, null).fold(
			"Test",
			(i, l) -> i + l,
			(i, r) -> i + r,
			(i, l, r) -> i + l + r,
			i -> i + "None"
		), equalTo("TestNone"));

	}

	@Test
	public void testFoldLeftNull() {
		assertThat(createTestCase(null, "Bar").fold(
			"Test",
			(i, l) -> i + l,
			(i, r) -> i + r,
			(i, l, r) -> i + l + r,
			i -> i + "None"
		), equalTo("TestBar"));

	}

	@Test
	public void testFoldRightNull() {
		assertThat(createTestCase("Foo", null).fold(
			"Test",
			(i, l) -> i + l,
			(i, r) -> i + r,
			(i, l, r) -> i + l + r,
			i -> i + "None"
		), equalTo("TestFoo"));
	}

	@Test
	public void testApplyLeft() {
		AtomicReference<String> ref = new AtomicReference<>();
		createTestCase("Foo", null)
			.ifLeft(l -> ref.set("Left"))
			.ifRight(l -> ref.set("Right"))
			.ifBoth((l, r) -> ref.set("Both"))
			.ifNone(() -> ref.set("None"));

		assertThat(ref.get(), equalTo("Left"));
	}

	@Test
	public void testApplyRight() {
		AtomicReference<String> ref = new AtomicReference<>();
		createTestCase(null, "Bar")
			.ifLeft(l -> ref.set("Left"))
			.ifRight(l -> ref.set("Right"))
			.ifBoth((l, r) -> ref.set("Both"))
			.ifNone(() -> ref.set("None"));

		assertThat(ref.get(), equalTo("Right"));
	}

	@Test
	public void testApplyBoth() {
		AtomicReference<String> ref = new AtomicReference<>();
		createTestCase("Foo", "Bar")
			.ifLeft(l -> ref.set("Left"))
			.ifRight(l -> ref.set("Right"))
			.ifBoth((l, r) -> ref.set("Both"))
			.ifNone(() -> ref.set("None"));

		assertThat(ref.get(), equalTo("Both"));
	}

	@Test
	public void testApplyNone() {
		AtomicReference<String> ref = new AtomicReference<>();
		createTestCase(null, null)
			.ifLeft(l -> ref.set("Left"))
			.ifRight(l -> ref.set("Right"))
			.ifBoth((l, r) -> ref.set("Both"))
			.ifNone(() -> ref.set("None"));

		assertThat(ref.get(), equalTo("None"));
	}

	@Test
	public void testReMapBothNonNull() {
		assertThat(createTestCase("Fooz", "Bar")
					   .map(String::length)
					   .map(
						   l -> l,
						   r -> r,
						   Integer::sum,
						   () -> 0
					   ), equalTo(7));

	}

	@Test
	public void testReMapBothNull() {
		assertThat(createTestCase(null, null)
					   .map(String::length)
					   .map(
						   l -> l,
						   r -> r,
						   Integer::sum,
						   () -> 0
					   ), equalTo(0));

	}

	@Test
	public void testReMapLeftNull() {
		assertThat(createTestCase(null, "Bar")
					   .map(String::length)
					   .map(
						   l -> l,
						   r -> r,
						   Integer::sum,
						   () -> 0
					   ), equalTo(3));

	}

	@Test
	public void testReMapRightNull() {
		assertThat(createTestCase("Fooz", null)
					   .map(String::length)
					   .map(
						   l -> l,
						   r -> r,
						   Integer::sum,
						   () -> 0
					   ), equalTo(4));
	}

	@Test
	public void testFilterBothNonNull() {
		assertThat(createTestCase("Foo", "Bar")
					   .filter(v -> true)
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("FooBar"));

		assertThat(createTestCase("Foo", "Bar")
					   .filter(v -> v.startsWith("F"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("Foo"));

		assertThat(createTestCase("Foo", "Bar")
					   .filter(v -> v.startsWith("B"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("Bar"));

		assertThat(createTestCase("Foo", "Bar")
					   .filter(v -> v.startsWith("Z"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("None"));
	}

	@Test
	public void testFilterBothNull() {
		assertThat(createTestCase(null, null)
					   .filter(v -> v.startsWith("F"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("None"));

	}

	@Test
	public void testFilterLeftNull() {
		assertThat(createTestCase(null, "Bar")
					   .filter(v -> v.startsWith("B"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("Bar"));

		assertThat(createTestCase(null, "Bar")
					   .filter(v -> v.startsWith("F"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("None"));

	}

	@Test
	public void testFilterRightNull() {
		assertThat(createTestCase("Fooz", null)
					   .filter(v -> v.startsWith("F"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("Fooz"));

		assertThat(createTestCase("Fooz", null)
					   .filter(v -> v.startsWith("B"))
					   .map(
						   l -> l,
						   r -> r,
						   (l,r) -> l + r,
						   () -> "None"
					   ), equalTo("None"));
	}

	private Both<String> createTestCase(@Nullable String left, @Nullable String right) {
		return both(left).and(right);
	}
}
