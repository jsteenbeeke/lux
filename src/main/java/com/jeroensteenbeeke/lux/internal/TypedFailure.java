package com.jeroensteenbeeke.lux.internal;

import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Failure result. Does not contain an object, but does contain a failure message
 * @param <T> The type of object normally contained in this result
 * @param message The failure message
 */
public final record TypedFailure<T>(@NotNull String message) implements TypedResult<T> {
	@Override
	public boolean isOk() {
		return false;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public T getObject() {
		throw new IllegalStateException("Failure results do not contain objects");
	}

	@Override
	public <TR extends Throwable> @NotNull T throwIfNotOk(@NotNull Function<String, TR> errorToException) throws TR {
		throw errorToException.apply(message);
	}

	@Override
	@NotNull
	public Stream<T> allResults() {
		return Stream.empty();
	}

	@Override
	@NotNull
	public <F> TypedResult<F> map(@NotNull FailableFunction<T, F> function) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public <I, F> TypedResult<F> fold(@NotNull I input, @NotNull FailableBiFunction<I, T, F> function) {
		return TypedResult.fail(message);
	}

	@Override
	public @NotNull <I, F> TypedResult<F> foldResult(@NotNull TypedResult<I> input, @NotNull FailableBiFunction<I, T, F> function) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public TypedResult<T> peek(@NotNull Consumer<T> consumer) {
		return this;
	}

	@Override
	@NotNull
	public <F, E extends RuntimeException> TypedResult<F> mapUnless(@NotNull Class<E> exceptionClass, @NotNull FailableFunction<T, F> function) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public <F> TypedResult<F> flatMap(@NotNull FailableFunction<T, TypedResult<F>> function) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public <F, E extends RuntimeException> TypedResult<F> flatMapUnless(@NotNull Class<E> exceptionClass, @NotNull FailableFunction<T, TypedResult<F>> function) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public ActionResult asSimpleResult() {
		return ActionResult.error(message);
	}

	@Override
	@NotNull
	public Optional<T> asOptional() {
		return Optional.empty();
	}

	@Override
	public T orElse(@NotNull Function<String, T> errorMessageToResult) {
		return errorMessageToResult.apply(message);
	}

	@Override
	@NotNull
	public TypedResult<T> filter(@NotNull Predicate<T> resultPredicate, @NotNull String failureMessage, @NotNull Object... messageParams) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public TypedResult<T> filter(@NotNull Predicate<T> resultPredicate, @NotNull Function<T, String> errorMessageProvider) {
		return TypedResult.fail(message);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TypedFailure<?> that)) return false;
		return message.equals(that.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(message);
	}
}
