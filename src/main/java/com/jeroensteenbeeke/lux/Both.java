package com.jeroensteenbeeke.lux;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;
import java.util.function.*;

/**
 * Construct for simplifying logic concerning two variables that
 * have differing behavior depending on which combination of variables
 * matches given conditions. Defaults to checking if the variables are non-null.
 * <p>
 * Immutable. Mutators create new instances
 *
 * @param <T> The type
 */
public class Both<T> {
	private final T left;

	private final T right;

	private final Predicate<T> condition;

	private Both(T left, T right, Predicate<T> condition) {
		this.left = left;
		this.right = right;
		this.condition = condition;
	}

	/**
	 * Create a new Both with the given left value
	 *
	 * @param left The left value
	 * @param <T>  The type of values
	 * @return A builder
	 */
	public static <T> And<T> both(@Nullable T left) {
		return right -> new Both<>(left, right, Objects::nonNull);
	}

	/**
	 * Builder
	 *
	 * @param <T> The type of value
	 */
	public interface And<T> {
		/**
		 * Create a new Both with the given right value
		 *
		 * @param right The right value
		 * @return An Both instance
		 */
		Both<T> and(@Nullable T right);
	}

	/**
	 * Modify the condition for both values
	 *
	 * @param condition The condition
	 * @return A new Both with the given condition
	 */
	public Both<T> withCondition(@NotNull Predicate<T> condition) {
		return new Both<>(left, right, condition);
	}

	/**
	 * Map the values depending on which of the values match the condition
	 *
	 * @param ifLeft  Function applied when only the left value matches the condition
	 * @param ifRight Function applied when only the right value matches the condition
	 * @param ifBoth  Function applied when both values match the condition
	 * @param ifNone  Supplier invoked when no value matches the condition
	 * @param <R>     The type of result
	 * @return A value of type R
	 */
	public <R> R map(Function<T, R> ifLeft, Function<T, R> ifRight, BiFunction<T, T, R> ifBoth, Supplier<R> ifNone) {
		boolean isLeft = condition.test(left);
		boolean isRight = condition.test(right);
		if (isLeft || isRight) {
			if (!isLeft) {
				return ifRight.apply(right);
			} else if (!isRight) {
				return ifLeft.apply(left);
			} else {
				return ifBoth.apply(left, right);
			}
		} else {
			return ifNone.get();
		}
	}

	/**
	 * Transforms any non-null values using the given function. Resetting
	 * the condition to default non-null check
	 *
	 * @param function The function to apply
	 * @param <R>      The type of value for the new Both
	 * @return A new both with the transformed values
	 */
	public <R> Both<R> map(Function<T, R> function) {
		return new Both<>(Optional.ofNullable(left).map(function).orElse(null),
						  Optional.ofNullable(right).map(function).orElse(null),
						  Objects::nonNull);
	}

	/**
	 * Filters this both's values using the giving predicate, yielding a new Both
	 * with possible different values
	 *
	 * @param function The function to apply
	 * @return A new both with the transformed values
	 */
	public Both<T> filter(Predicate<T> function) {
		return new Both<>(Optional.ofNullable(left).filter(function).orElse(null),
						  Optional.ofNullable(right).filter(function).orElse(null),
						  condition);
	}

	/**
	 * Takes the given input and applies one of the given functions depending on which conditions are satisfied
	 *
	 * @param input   The input to use as basis for the functions
	 * @param ifLeft  Function applied when only the left value matches the condition
	 * @param ifRight Function applied when only the right value matches the condition
	 * @param ifBoth  Function applied when both values match the condition
	 * @param ifNone  Supplier invoked when no value matches the condition
	 * @param <R>     The type of input
	 * @param <V>     The type of result
	 * @return A value of type V
	 */
	@NotNull
	public <R, V> V fold(@NotNull R input,
						 @NotNull BiFunction<R, T, V> ifLeft,
						 @NotNull BiFunction<R, T, V> ifRight,
						 @NotNull TriFunction<R, T, T, V> ifBoth,
						 @NotNull Function<R, V> ifNone) {
		boolean isLeft = condition.test(left);
		boolean isRight = condition.test(right);
		if (isLeft || isRight) {
			if (!isLeft) {
				return ifRight.apply(input, right);
			} else if (!isRight) {
				return ifLeft.apply(input, left);
			} else {
				return ifBoth.apply(input, left, right);
			}
		} else {
			return ifNone.apply(input);
		}
	}

	/**
	 * Passes the left value to the given consumer if only the left value matches the condition
	 *
	 * @param ifLeft The consumer to apply
	 * @return The current object
	 */
	public Both<T> ifLeft(Consumer<T> ifLeft) {
		boolean isLeft = condition.test(left);
		boolean isRight = condition.test(right);
		if (isLeft && !isRight) {
			ifLeft.accept(left);
		}
		return this;
	}

	/**
	 * Passes the right value to the given consumer if only the right value matches the condition
	 *
	 * @param ifRight The consumer to apply
	 * @return The current object
	 */
	public Both<T> ifRight(Consumer<T> ifRight) {
		boolean isLeft = condition.test(left);
		boolean isRight = condition.test(right);
		if (!isLeft && isRight) {
			ifRight.accept(right);
		}
		return this;
	}

	/**
	 * Passes both values to the given consumer if they both match the condition
	 *
	 * @param ifBoth The consumer to apply
	 * @return The current object
	 */
	public Both<T> ifBoth(BiConsumer<T, T> ifBoth) {
		boolean isLeft = condition.test(left);
		boolean isRight = condition.test(right);
		if (isLeft && isRight) {
			ifBoth.accept(left, right);
		}
		return this;
	}

	/**
	 * Invokes the given runnable if no values match the condition
	 *
	 * @param ifNone The runnable to run
	 * @return The current object
	 */
	public Both<T> ifNone(Runnable ifNone) {
		boolean isLeft = condition.test(left);
		boolean isRight = condition.test(right);
		if (!isLeft && !isRight) {
			ifNone.run();
		}
		return this;
	}

	/**
	 * Function that accepts 3 parameters and yields a single object
	 *
	 * @param <T> The first parameter type
	 * @param <U> The second parameter type
	 * @param <V> The third parameter type
	 * @param <W> The return type
	 */
	@FunctionalInterface
	public interface TriFunction<T, U, V, W> {
		/**
		 * Executes the function
		 *
		 * @param t First parameter
		 * @param u Second parameter
		 * @param v Third parameter
		 * @return The function result
		 */
		@NotNull
		W apply(@NotNull T t, @NotNull U u, @NotNull V v);
	}
}
