/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.lux;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Implementation of the Result interface that combines the functionality of Result (status ok or error message)
 * with an encapsulated result object (like a {@code java.util.Optional}).
 *
 * @param <T> The type of the contained result
 */
public class TypedResult<T> implements Result<TypedResult<T>,T> {
	private final T object;

	private final String message;

	TypedResult(String message, T object) {
		this.object = object;
		this.message = message;
	}

	/**
	 * @see Result
	 */
	@Override
	public boolean isOk() {
		return object != null;
	}

	/**
	 * Get the encapsulated object, if it exists. Please keep in mind that this method
	 * will only return an object if the {@code isOk()} method returns {@code true}
	 *
	 * @return The encapsulated object, if it exists
	 */
	public T getObject() {
		return object;
	}

	/**
	 * @see Result
	 */
	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public <TR extends Throwable> T throwIfNotOk(Function<String, TR> errorToException) throws TR {
		if (isOk()) {
			return getObject();
		}

		throw errorToException.apply(getMessage());
	}

	/**
	 * Create a stream of the contained object, if applicable
	 *
	 * @return An empty stream (if {@code isOk()} returns {@code false}), or a stream containing just the contained
	 * object otherwise
	 */
	public Stream<T> allResults() {
		if (!isOk()) {
			return Stream.empty();
		}

		return Stream.of(object);
	}

	/**
	 * Attempts to map the encapsulated object using the given function. If the current result is
	 * already in an error state, it will simply pass along the error state to another TypedResult of the target
	 * type. If the current result is in an OK state, however, it will attempt to apply the function. If the
	 * function throws an exception, this will result in failed TypedResult being returned. If the function
	 * returns correctly, a TypedResult containing the mapped result will be returned
	 *
	 * @param function The function to map the currently contained result to the target type
	 * @param <F>      The target type
	 * @return A TypedResult containing the mapped value, or an error message
	 */
	public <F> TypedResult<F> map(FailableFunction<T, F> function) {
		if (isOk()) {
			return attempt(() -> function.apply(getObject()));
		}

		return fail(getMessage());
	}

	/**
	 * Inspect the intermediary result, if present
	 *
	 * @param consumer The consumer to inspect the contained value
	 * @return A TypedResult containing the mapped value, or an error message
	 */
	public TypedResult<T> peek(Consumer<T> consumer) {
		if (isOk()) {
			consumer.accept(getObject());
		}

		return this;
	}

	/**
	 * Attempts to map the encapsulated object using the given function. If the current result is
	 * already in an error state, it will simply pass along the error state to another TypedResult of
	 * the target type. If the current result is in an OK state, however, it will attempt to apply the function. If the
	 * function throws an exception, this will result in failed TypedResult being returned,
	 * unless the type of the exception is assignable from the given exception class, in which
	 * case the exception will be thrown. If the function
	 * returns correctly, a TypedResult containing the mapped result will be returned
	 *
	 * @param exceptionClass The class of exception that should be thrown rather than caught
	 * @param function The function to map the currently contained result to the target type
	 * @param <F>      The target type
	 * @param <E> The type of exception we should throw rather than wrap
	 * @return A TypedResult containing the mapped value, or an error message
	 */
	public <F, E extends RuntimeException> TypedResult<F> mapUnless(Class<E> exceptionClass,
																	FailableFunction<T, F>
																			function) {
		if (isOk()) {
			try {
				return ok(function.apply(object));
			} catch (Exception e) {
				if (exceptionClass.isAssignableFrom(e.getClass())) {
					throw (RuntimeException) e;
				}
				return fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
			}
		}
		return fail(getMessage());
	}

	/**
	 * Attempts to map the encapsulated object using the given function. If the current result is
	 * already in an error state, it will simply pass along the error state to another TypedResult of the target
	 * type. If the current result is in an OK state, however, it will attempt to apply the function. If the
	 * function throws an exception, this will result in failed TypedResult being returned,
	 * unless the type of the exception is assignable from the given exception class, in which
	 * case the exception is thrown. If the function returns correctly, a TypedResult containing
	 * the mapped result will be returned.
	 * <p>
	 * This function differs from the {@code map()} method in that the function should return a TypedResult rather
	 * than just a result object.
	 *
	 * @param function The function to map the currently contained result to a TypedResult
	 * @param <F>      The target type
	 * @return A TypedResult containing the mapped value, or an error message
	 */
	public <F> TypedResult<F> flatMap(FailableFunction<T, TypedResult<F>> function) {
		if (isOk()) {
			try {
				return function.apply(getObject());
			} catch (Exception e) {
				return fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
			}
		}

		return fail(getMessage());
	}

	/**
	 * Attempts to map the encapsulated object using the given function. If the current result is
	 * already in an error state, it will simply pass along the error state to another TypedResult of the target
	 * type. If the current result is in an OK state, however, it will attempt to apply the function. If the
	 * function throws an exception, this will result in failed TypedResult being returned. If the function
	 * returns correctly, a TypedResult containing the mapped result will be returned.
	 * <p>
	 * This function differs from the {@code map()} method in that the function should return a TypedResult rather
	 * than just a result object.
	 *
	 * @param exceptionClass The class of exception that should be thrown rather than caught
	 * @param function The function to map the currently contained result to a TypedResult
	 * @param <F>      The target type
	 * @param <E> The type of exception we should throw rather than wrap
	 * @return A TypedResult containing the mapped value, or an error message
	 */
	public <F,E extends RuntimeException> TypedResult<F> flatMapUnless(Class<E> exceptionClass,
											FailableFunction<T,TypedResult<F>> function) {
		if (isOk()) {
			try {
				return function.apply(getObject());
			} catch (Exception e) {
				if (exceptionClass.isAssignableFrom(e.getClass())) {
					throw (RuntimeException) e;
				}
				return fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
			}
		}

		return fail(getMessage());
	}

	/**
	 * Creates a failure based on the given ActionResult
	 *
	 * @param failure The failed ActionResult.
	 * @param <T>     The type of the contained result object. Seeing as we're creating a failure
	 *                there won't actually be an instance of this object contained, but the calling code
	 *                may have one defined regardless.
	 * @return A TypedResult with the same error message as the given failure
	 * @throws IllegalStateException If the failure isn't actually a failure but a success.
	 */
	public static <T> TypedResult<T> fail(ActionResult failure) {
		if (failure.isOk()) {
			throw new IllegalArgumentException(
					"Cannot turn a successful result into a failure");
		}

		return fail(failure.getMessage());

	}

	/**
	 * Attempts to perform the given action. If the attempt succeeds (i.e. no exception is thrown), the result
	 * is encapsulated in a TypedResult as an ok status. If an exception is thrown, the message of the exception
	 * is wrapped as a failure result
	 *
	 * @param attempt The attempt to perform
	 * @param <T>     The type of the attempt's return value
	 * @return A TypedResult either containing the attempt's result, or a message of why it failed
	 */
	public static <T> TypedResult<T> attempt(Attempt<T> attempt) {
		try {
			return ok(attempt.perform());
		} catch (Exception e) {
			return fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
		}
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TypedResult<?> that = (TypedResult<?>) o;

		if (object != null ? !object.equals(that.object) : that.object != null) return false;
		return message != null ? message.equals(that.message) : that.message == null;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = object != null ? object.hashCode() : 0;
		result = 31 * result + (message != null ? message.hashCode() : 0);
		return result;
	}

	/**
	 * Create a new successful TypedResult for the given result object
	 * @param object The result object
	 * @param <T> The type of the result object
	 * @return A TypedResult with status ok, containing the given object
	 */
	public static <T> TypedResult<T> ok(T object) {
		return new TypedResult<>(null, object);
	}

	/**
	 * Creates a new TypedResult in error state, with the given formatted error message
	 * @param format Either the error message (if {@code params.length == 0}), or a String format to compose the error message from
	 * @param params The parameters to the String formatter
	 * @param <T> The type of the result object, if this result had been successful
	 * @return A TypedResult with status failure, containing the formatted error message
	 * @see java.lang.String#format(String,Object...)
	 */
	public static <T> TypedResult<T> fail(String format,
										  Object... params) {
		if (params.length == 0) {
			return new TypedResult<>(format, null);
		}

		return new TypedResult<>(String.format(format, params),
				null);
	}

	/**
	 * Takes the current result and simplifies it, discarding the result object
	 * @return An ActionResult containing either an ok status, or the same error messages as the current result
	 */
	public ActionResult asSimpleResult() {
		return isOk() ? ActionResult.ok() : ActionResult.error(getMessage());
	}

	/**
	 * Returns the current result as an optional, assuming it has a result. Generally speaking
	 * you only want to invoke this method once you've already done something with any
	 * present error messages (such as {@link #ifNotOk(Consumer)})
	 * @return An optional for this result's object
	 */
	public Optional<T> asOptional() {
		return Optional.ofNullable(getObject());
	}

	/**
	 * Force the TypedResult to return a value of the class's generic type, using the given
	 * function to map any present error messages to a valid result. Similar in concept to {@link java.util.Optional#orElse(Object)}
	 *
	 * @param errorMessageToResult A function that maps an error message to valid result
	 * @return A valid object of type {@link T}
	 */
	public T orElse(Function<String,T> errorMessageToResult) {
		if (isOk()) {
			return getObject();
		} else {
			return errorMessageToResult.apply(getMessage());
		}
	}

	/**
	 * Applies a filter to the current result. If the current result is already a failure, this failure remains the
	 * current state. If the current result is ok, the predicate is applied. If it matches, the current state is returned,
	 * and if it does not, a new failure state is constructed with the given message and message parameters
	 * @param resultPredicate The predicate to apply to the result
	 * @param failureMessage The message (format) to use when the predicate does not hold
	 * @param messageParams Parameters to pass to the String formatter
	 * @return A result object of the same type as the current object
	 */
	public TypedResult<T> filter(Predicate<T> resultPredicate, String failureMessage, Object... messageParams) {
		if (isOk()) {
			T result = getObject();

			if (resultPredicate.test(result)) {
				return this;
			} else {
				return fail(failureMessage, messageParams);
			}
		}

		return this;
	}

	/**
	 * Applies a filter to the current result. If the current result is already a failure, this failure remains the
	 * current state. If the current result is ok, the predicate is applied. If it matches, the current state is returned,
	 * and if it does not, a new failure state is constructed with the given message and message parameters
	 * @param resultPredicate The predicate to apply to the result
	 * @param errorMessageProvider A function that takes the previous result object and uses it to create an error message
	 * @return A result object of the same type as the current object
	 */
	public TypedResult<T> filter(Predicate<T> resultPredicate, Function<T,String> errorMessageProvider) {
		if (isOk()) {
			T result = getObject();

			if (resultPredicate.test(result)) {
				return this;
			} else {
				return fail(errorMessageProvider.apply(result));
			}
		}

		return this;
	}

	/**
	 * Supplier class that allows for the throwing of exceptions. Used in {@code TypedResult.attempt}
	 *
	 * @param <T> The result type returned by the interface's method
	 */
	@FunctionalInterface
	public interface Attempt<T> {
		/**
		 * Perform the given attempt
		 *
		 * @return The result of the attempt
		 * @throws Exception If the attempt fails
		 */
		T perform() throws Exception;
	}

	/**
	 * Function whose execution can fail
	 *
	 * @param <T> The type of input
	 * @param <R> The type of output
	 */
	@FunctionalInterface
	public interface FailableFunction<T, R> {
		/**
		 * Apply this function's logic to the given input, yielding an output of the indicated type
		 *
		 * @param input The input to perform the logic on
		 * @return The result of the function, if successful
		 * @throws Exception If the function fails
		 */
		R apply(T input) throws Exception;
	}

}
