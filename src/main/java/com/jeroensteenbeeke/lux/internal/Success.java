package com.jeroensteenbeeke.lux.internal;

import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;
import java.util.function.Supplier;

public enum Success implements ActionResult {
	INSTANCE;

	@Override
	public boolean isOk() {
		return true;
	}

	@Override
	public String getMessage() {
		throw new IllegalStateException("Success results have no message");
	}

	@Override
	@NotNull
	public <T> TypedResult<T> createIfOk(@NotNull Supplier<T> objectSupplier) {
		return TypedResult.ok(objectSupplier.get());
	}

	@Override
	@NotNull
	public <TR extends Throwable> ActionResult throwIfNotOk(@NotNull Function<String, TR> errorToException) throws TR {
		return this;
	}

	@Override
	@NotNull
	public ActionResult andThen(@NotNull Supplier<ActionResult> actionResultSupplier) {
		return actionResultSupplier.get();
	}
}
