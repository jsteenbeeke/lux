package com.jeroensteenbeeke.lux.internal;

import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Failure result. Has a reason for failure embedded
 *
 * @param message The failure message
 */
public record Failure(@NotNull String message) implements ActionResult {
	@Override
	public boolean isOk() {
		return false;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	@NotNull
	public <T> TypedResult<T> createIfOk(@NotNull Supplier<T> objectSupplier) {
		return TypedResult.fail(message);
	}

	@Override
	@NotNull
	public <TR extends Throwable> ActionResult throwIfNotOk(@NotNull Function<String, TR> errorToException) throws TR {
		throw errorToException.apply(message);
	}

	@Override
	@NotNull
	public ActionResult andThen(@NotNull Supplier<ActionResult> actionResultSupplier) {
		return this;
	}
}
