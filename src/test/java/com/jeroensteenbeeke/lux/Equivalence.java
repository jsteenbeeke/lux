package com.jeroensteenbeeke.lux;

import org.jetbrains.annotations.NotNull;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Utility class for testing object equivalence through their equals method
 */
class Equivalence {

	private static final String TRANSITIVE_REQUIREMENT =
			"To properly test transitive equality, please create 3 distinct objects that are functionally equal";

	private Equivalence() {

	}

	/**
	 * Ensures that two objects are symmetrically equivalent and produce the same hashcode
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @throws AssertionError If the objects are not symmetrically equivalent, or, barring that, their hashcodes differ
	 */
	public static void properHashcode(@NotNull Object a, @NotNull Object b) {
		symmetric(a, b);

		assertEquals(a.hashCode(), b.hashCode(), "Two symmetrically equivalent objects have different hashcodes");
	}

	/**
	 * Ensures that the three given objects are transitively equivalent (that is, if a equals b, and b equals c, then
	 * a must equal c). This method requires the three parameters to be distinct objects
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @param c The third object to check
	 * @throws AssertionError If the objects are not distinct, or if they are not transitively equivalent
	 */
	public static void transitive(@NotNull Object a, @NotNull Object b, @NotNull Object c) {
		assertNotSame(a, b, TRANSITIVE_REQUIREMENT);
		assertNotSame(b, c, TRANSITIVE_REQUIREMENT);
		assertNotSame(a, c, TRANSITIVE_REQUIREMENT);

		assertEquals(a, b, "Transitivity: A does not equal B");
		assertEquals(b, c, "Transitivity: B does not equal C");
		assertEquals(a, c, "Transitivity: A does not equal C");
	}

	/**
	 * Ensures that two objects are symmetrically equivalent (that is: if a equals b, then b must be equal to a)
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @throws AssertionError If the objects are not distinct, or if they are not symmetrically equivalent
	 */
	public static void symmetric(Object a, Object b) {
		assertEquals(a, b, "Symmetry: A does not equal B");
		assertEquals(b, a, "Symmetry: B does not equal A");
	}

	/**
	 * Ensures that the given object is reflexively equivalent. That is, it must be equal to itself
	 *
	 * @param a The object to check
	 * @throws AssertionError If the object is not reflexively equivalent
	 */
	public static void reflexive(Object a) {
		assertEquals(a, a, "Reflexivity: A does not equal A");
	}

	/**
	 * Ensures that the given three objects are equivalent in a variety of ways. They are tested for reflexivity,
	 * symmetry and transitivity, and their
	 * hashcodes are also checked
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @param c The third object to check
	 * @throws AssertionError If any two objects are not fully equivalents
	 */
	public static void equivalent(Object a, Object b, Object c) {
		reflexive(a);
		reflexive(b);
		reflexive(c);

		symmetric(a, b);
		symmetric(b, c);

		transitive(a, b, c);
		transitive(c, b, a);

		properHashcode(a, b);
		properHashcode(b, c);
		properHashcode(a, c);
	}

	/**
	 * Tests that two object are not equivalent in a symmetric way.
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @throws AssertionError If a is equal to b, or if b is equal to a
	 */
	public static void nonSymmetric(Object a, Object b) {
		assertNotEquals(a, b, "Symmetry: A does not equal B");
		assertNotEquals(b, a, "Symmetry: B does not equal A");
	}
}
