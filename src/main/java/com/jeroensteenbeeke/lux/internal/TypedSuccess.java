package com.jeroensteenbeeke.lux.internal;

import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Success result. Contains a result object, but no message
 *
 * @param <T> The type of object contained in the result
 * @param object The success object
 */
public final record TypedSuccess<T>(@NotNull T object) implements TypedResult<T> {
	@Override
	public boolean isOk() {
		return true;
	}

	@Override
	public String getMessage() {
		throw new IllegalStateException("Success results do not have failure messages");
	}

	@Override
	public T getObject() {
		return object;
	}

	@Override
	@NotNull
	public <TR extends Throwable> T throwIfNotOk(@NotNull Function<String, TR> errorToException) {
		return getObject();
	}

	@Override
	@NotNull
	public Stream<T> allResults() {
		return Stream.of(getObject());
	}

	@Override
	@NotNull
	public <F> TypedResult<F> map(@NotNull FailableFunction<T, F> function) {
		return TypedResult.attempt(() -> function.apply(getObject()));
	}

	@Override
	@NotNull
	public <I, F> TypedResult<F> fold(@NotNull I input, @NotNull FailableBiFunction<I, T, F> function) {
		return TypedResult.attempt(() -> function.apply(input, getObject()));
	}

	@Override
	@NotNull
	public <I, F> TypedResult<F> foldResult(@NotNull TypedResult<I> input, @NotNull FailableBiFunction<I, T, F> function) {
		if (input.isOk()) {
			return TypedResult.attempt(() -> function.apply(input.getObject(), getObject()));
		} else {
			return TypedResult.fail(input.getMessage());
		}
	}

	@Override
	@NotNull
	public TypedResult<T> peek(@NotNull Consumer<T> consumer) {
		consumer.accept(object);
		return this;
	}

	@Override
	public @NotNull <F, E extends RuntimeException> TypedResult<F> mapUnless(@NotNull Class<E> exceptionClass, @NotNull FailableFunction<T, F> function) {
		try {
			return TypedResult.ok(function.apply(object));
		} catch (Exception e) {
			if (exceptionClass.isAssignableFrom(e.getClass())) {
				throw (RuntimeException) e;
			}
			return TypedResult.fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
		}
	}

	@Override
	@NotNull
	public <F> TypedResult<F> flatMap(@NotNull FailableFunction<T, TypedResult<F>> function) {
		try {
			return function.apply(getObject());
		} catch (Exception e) {
			return TypedResult.fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
		}
	}

	@Override
	@NotNull
	public <F, E extends RuntimeException> TypedResult<F> flatMapUnless(@NotNull Class<E> exceptionClass, @NotNull FailableFunction<T, TypedResult<F>> function) {
		try {
			return function.apply(getObject());
		} catch (Exception e) {
			if (exceptionClass.isAssignableFrom(e.getClass())) {
				throw (RuntimeException) e;
			}
			return TypedResult.fail("%s: %s", e.getClass().getSimpleName(), e.getMessage());
		}
	}

	@Override
	@NotNull
	public ActionResult asSimpleResult() {
		return ActionResult.ok();
	}

	@Override
	@NotNull
	public Optional<T> asOptional() {
		return Optional.of(getObject());
	}

	@Override
	@NotNull
	public T orElse(@NotNull Function<String, T> errorMessageToResult) {
		return getObject();
	}

	@Override
	@NotNull
	public TypedResult<T> filter(@NotNull Predicate<T> resultPredicate, @NotNull String failureMessage, @NotNull Object... messageParams) {
		if (resultPredicate.test(getObject())) {
			return this;
		} else {
			return TypedResult.fail(failureMessage, messageParams);
		}
	}

	@Override
	@NotNull
	public TypedResult<T> filter(@NotNull Predicate<T> resultPredicate, @NotNull Function<T, String> errorMessageProvider) {
		T result = getObject();

		if (resultPredicate.test(result)) {
			return this;
		} else {
			return TypedResult.fail(errorMessageProvider.apply(result));
		}
	}
}
