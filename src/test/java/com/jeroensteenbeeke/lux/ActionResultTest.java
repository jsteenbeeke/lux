package com.jeroensteenbeeke.lux;


import com.jeroensteenbeeke.lux.internal.Failure;
import com.jeroensteenbeeke.lux.internal.TypedFailure;
import com.jeroensteenbeeke.lux.internal.TypedSuccess;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static com.jeroensteenbeeke.lux.Equivalence.reflexive;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class ActionResultTest {
	@Test
	public void testRegularOkActionResult() {
		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		ActionResult yes = ActionResult.ok();

		assertTrue(yes.isOk());

		yes.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set);

		assertTrue(yesBranch.get());
		assertNull(noReason.get());

		yes.allOk().forEach(ar -> assertTrue(ar.isOk()));
		// If this stream returns any elements, the assertFalse will fail since we've already established the result
		// to be true, and the stream should never have more than 1 element
		yes.allNotOk().forEach(ar -> assertFalse(ar.isOk()));

		// Throws an exception if result is not ok, which should not happen, therefore no catch statement
		yes.throwIfNotOk(IllegalStateException::new);
	}

	@Test
	public void testRegularFailActionResult() {
		final String reason = "This is a testcase";

		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		ActionResult no = ActionResult.error(reason);

		assertFalse(no.isOk());

		no.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set);

		assertFalse(yesBranch.get());
		assertEquals(reason, noReason.get());
		assertEquals(no.getMessage(), noReason.get());

		no.allOk().forEach(ar -> assertTrue(ar.isOk()));
		no.allNotOk().forEach(ar -> assertFalse(ar.isOk()));

		try {
			no.throwIfNotOk(IllegalStateException::new);
			// If the previous statement works correctly, the next line should not be reached
			assertTrue(no.isOk());
		} catch (IllegalStateException e) {
			assertEquals(no.getMessage(), e.getMessage());
		}
	}

	@Test
	public void testRegularChaining() {
		assertFalse(ActionResult.error("Error").andThen(ActionResult::ok).isOk());
		assertFalse(ActionResult.ok().andThen(() -> ActionResult.error("Error")).isOk());
		assertTrue(ActionResult.ok().andThen(ActionResult::ok).isOk());
		assertFalse(ActionResult.error("Error").andThen(() -> ActionResult.error("Error")).isOk());
		assertEquals("Error", ActionResult.error("Error").andThen(() -> ActionResult.error("Error2")).getMessage());
	}

	@Test
	public void testRegularFailActionResultWithStringFormat() {
		final String reason = "This is a testcase %s";
		final String suffix = "too";

		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		ActionResult no = ActionResult.error(reason, suffix);

		assertFalse(no.isOk());

		no.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set);

		assertFalse(yesBranch.get());
		assertEquals(String.format(reason, suffix), noReason.get());
		assertEquals(no.getMessage(), noReason.get());

		no.allOk().forEach(ar -> assertTrue(ar.isOk()));
		no.allNotOk().forEach(ar -> assertFalse(ar.isOk()));

		try {
			no.throwIfNotOk(IllegalStateException::new);
			// If the previous statement works correctly, the next line should not be reached
			assertTrue(no.isOk());
		} catch (IllegalStateException e) {
			assertEquals(no.getMessage(), e.getMessage());
		}
	}

	@Test
	public void testTypedOkActionResult() {
		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		TypedResult<Boolean> yes = TypedResult.ok(true);

		assertTrue(yes.isOk());

		yes.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set);

		assertTrue(yesBranch.get());
		assertNull(noReason.get());
		assertNotNull(yes.getObject());
		assertTrue(yes.getObject());

		yes.allOk().forEach(ar -> assertNotNull(ar.isOk()));
		yes.allOk().forEach(ar -> assertNotNull(ar.getObject()));
		// If this stream returns any elements, the assertFalse will fail since we've already established the result
		// to be true, and the stream should never have more than 1 element
		yes.allNotOk().forEach(ar -> assertFalse(ar.isOk()));
		yes.allResults().forEach(Assertions::assertNotNull);

		assertTrue(yes.orElse(msg -> false));

		// Throws an exception if result is not ok, which should not happen, therefore no catch statement
		Boolean optionalYes = yes.throwIfNotOk(IllegalStateException::new);

		assertEquals(true, optionalYes);
	}

	@Test
	public void testTypedFailActionResult() {
		final String reason = "This is a testcase";

		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		TypedResult<Boolean> no = TypedResult.fail(reason);

		assertFalse(no.isOk());

		Optional<Boolean> optionalNo = no.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set).asOptional();

		assertFalse(yesBranch.get());
		assertEquals(reason, noReason.get());
		assertEquals(no.getMessage(), noReason.get());
		assertFalse(optionalNo.isPresent());

		no.allOk().forEach(ar -> assertTrue(ar.isOk()));
		no.allNotOk().forEach(ar -> assertFalse(ar.isOk()));
		no.allResults().forEach(Assertions::assertNotNull);

		try {
			no.throwIfNotOk(IllegalStateException::new);
			// If the previous statement works correctly, the next line should not be reached
			assertTrue(no.isOk());
		} catch (IllegalStateException e) {
			assertEquals(no.getMessage(), e.getMessage());
		}

		assertTrue(no.orElse(msg -> true));


	}



	@Test
	public void testTypedFailActionResultWithStringFormat() {
		final String reason = "This is a testcase %s";
		final String suffix = "too";

		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		TypedResult<Boolean> no = TypedResult.fail(reason, suffix);

		assertFalse(no.isOk());

		no.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set);

		assertFalse(yesBranch.get());
		assertEquals(String.format(reason, suffix), noReason.get());
		assertEquals(no.getMessage(), noReason.get());

		no.allOk().forEach(ar -> assertTrue(ar.isOk()));
		no.allNotOk().forEach(ar -> assertFalse(ar.isOk()));
		no.allResults().forEach(Assertions::assertNotNull);

		try {
			no.throwIfNotOk(IllegalStateException::new);
			// If the previous statement works correctly, the next line should not be reached
			assertTrue(no.isOk());
		} catch (IllegalStateException e) {
			assertEquals(no.getMessage(), e.getMessage());
		}
	}

	@Test
	public void testRegularToTypedFailActionResult() {
		final String reason = "This is a testcase";

		AtomicBoolean yesBranch = new AtomicBoolean(false);
		AtomicReference<String> noReason = new AtomicReference<>(null);

		TypedResult<Boolean> no = TypedResult.fail(ActionResult.error(reason));

		assertFalse(no.isOk());

		no.ifOk(() -> yesBranch.set(true)).ifNotOk(noReason::set);

		assertFalse(yesBranch.get());
		assertEquals(reason, noReason.get());
		assertEquals(no.getMessage(), noReason.get());

		no.allOk().forEach(ar -> assertTrue(ar.isOk()));
		no.allNotOk().forEach(ar -> assertFalse(ar.isOk()));
		no.allResults().forEach(Assertions::assertNotNull);

		try {
			no.throwIfNotOk(IllegalStateException::new);
			// If the previous statement works correctly, the next line should not be reached
			assertTrue(no.isOk());
		} catch (IllegalStateException e) {
			assertEquals(no.getMessage(), e.getMessage());
		}
	}

	@Test
	public void testTypedChaining() {
		TypedResult<Long> no =
				TypedResult.attempt(() -> Integer.parseInt("5")).map(i -> (long) i).map(l -> Long.toString(l))
						.map(s -> s.concat(" dot 5")).map(Long::parseLong);

		assertFalse(no.isOk());

		TypedResult<String> aborted =
				TypedResult.attempt(() -> Integer.parseInt("5")).map(i -> (long) i).map(l -> Long.toString(l))
						.map(s -> s.concat(" dot 5")).map(Long::parseLong).map(l -> Long.toString(l));

		assertFalse(aborted.isOk());
		assertEquals("NumberFormatException: For input string: \"5 dot 5\"", aborted.getMessage());

		aborted =
				TypedResult.attempt(() -> Integer.parseInt("5")).map(i -> (long) i)
						.flatMap(l -> TypedResult.ok(Long.toString(l)))
						.flatMap(s -> TypedResult.ok(s.concat(" dot 5")))
						.flatMap(s -> TypedResult.ok(Long.parseLong(s))).flatMap(l -> TypedResult.ok(Long.toString(l)));

		assertFalse(aborted.isOk());

		final var intResult = ActionResult.ok().createIfOk(() -> 5);
		assertTrue(intResult.isOk());
		assertThrows(IllegalStateException.class, intResult::getMessage);
		assertEquals(5, (int) intResult.getObject());
		assertTrue(intResult.asSimpleResult().isOk());
		assertThrows(IllegalStateException.class, () -> intResult.asSimpleResult().getMessage());

		final var intResult2 = ActionResult.error("No!").createIfOk(() -> 8);
		assertFalse(intResult2.isOk());
		assertThrows(IllegalStateException.class, intResult2::getObject);

		assertEquals("No!", intResult2.getMessage());
		assertFalse(intResult2.asSimpleResult().isOk());
		assertEquals("No!", intResult2.asSimpleResult().getMessage());

		assertDoesNotThrow(() -> {
			TypedResult<Integer> result = TypedResult.attempt(() -> "five")
													 .mapUnless(
															 IllegalStateException
																	 .class,
															 Integer::parseInt);
			assertEquals("NumberFormatException: For input string: \"five\"", result.getMessage());
			result
					.ifOk(() -> { throw new IllegalStateException(); });
		});

		assertThrows(IllegalStateException.class, () -> TypedResult.attempt(() -> "5")
															   .mapUnless(IllegalStateException.class, Integer::parseInt)
															   .ifOk(() -> { throw new IllegalStateException(); }));


		assertThrows(NumberFormatException.class, () -> TypedResult.attempt(() -> "five")
																   .mapUnless(ArithmeticException.class, s -> s)
																   .mapUnless(NumberFormatException.class, Integer::parseInt)
																   .ifNotOk(e -> { throw new IllegalStateException(e); }));

		assertThrows(IllegalStateException.class, () -> TypedResult.attempt(() -> "five")
											.mapUnless(ArithmeticException.class, (TypedResult.FailableFunction<String,String>) s
													-> {
												throw new NumberFormatException();
											})
											.mapUnless(NumberFormatException.class, Integer::parseInt)
											.ifNotOk(e -> { throw new IllegalStateException(e); }));

		assertDoesNotThrow(() -> {
			TypedResult<Integer> result = TypedResult.attempt(() -> "five")
													 .flatMapUnless(
															 IllegalStateException
																	 .class,
															 s -> TypedResult.ok(Integer.parseInt
																								(s)));
			assertEquals("NumberFormatException: For input string: \"five\"", result.getMessage());
			result
					.ifOk(() -> { throw new IllegalStateException(); });
		});

		assertThrows(IllegalStateException.class, () -> TypedResult.attempt(() -> "5")
															   .flatMapUnless(IllegalStateException.class, s -> TypedResult.ok(Integer.parseInt
																								  (s)))
															   .ifOk(() -> { throw new IllegalStateException(); }));

		assertThrows(NumberFormatException.class, () -> TypedResult.attempt(() -> "five")
															   .flatMapUnless(ArithmeticException.class, TypedResult::ok)
															   .flatMapUnless(NumberFormatException.class, s -> TypedResult.ok(Integer.parseInt
																								  (s)))
															   .ifNotOk(e -> { throw new IllegalStateException(e); }));


		assertThrows(IllegalStateException.class, () -> TypedResult.attempt(() -> "five")
															   .flatMapUnless(ArithmeticException.class, (TypedResult
						   .FailableFunction<String,TypedResult<String>>) s
						   -> {
					   throw new NumberFormatException();
				   })
															   .flatMapUnless(NumberFormatException.class, s -> TypedResult.ok(Integer.parseInt
																								  (s)))
															   .ifNotOk(e -> { throw new IllegalStateException(e); }));

		assertThat(TypedResult.ok("Bar")
				   .fold("Foo", String::concat)
				   .orElse(error -> "Wrong"), equalTo("FooBar"));

		assertThat(TypedResult.<String> fail("Bar")
							  .fold("Foo", String::concat)
							  .orElse(error -> error), equalTo("Bar"));

		assertThat(TypedResult.ok("Bar")
							  .foldResult(TypedResult.ok("Foo"), String::concat)
							  .orElse(error -> "Wrong"), equalTo("FooBar"));

		assertThat(TypedResult.<String> fail("Bar")
					   .foldResult(TypedResult.ok("Foo"), String::concat)
					   .orElse(error -> error), equalTo("Bar"));

		assertThat(TypedResult.ok("Bar")
							  .foldResult(TypedResult.fail("Foo"), String::concat)
							  .orElse(error -> "Wrong"), equalTo("Wrong"));

		assertThat(TypedResult.<String> fail("Bar")
					   .foldResult(TypedResult.fail("Foo"), String::concat)
					   .orElse(error -> error), equalTo("Bar"));
	}


	@Test
	public void testResultEquality() {
		TypedResult<Integer> likeable = TypedResult.fail("I don't like you");
		TypedResult<Object> puppies = TypedResult.fail("Puppies in space");
		TypedResult<Object> puppies2 = TypedResult.fail("Puppies in space");
		TypedResult<Object> puppies3 = TypedResult.fail("Puppies in space");

		Equivalence.nonSymmetric(likeable, puppies);
		Equivalence.transitive(puppies, puppies2, puppies3);
		Equivalence.symmetric(puppies, puppies2);
		Equivalence.symmetric(puppies2, puppies3);
		Equivalence.symmetric(puppies, puppies3);

		Set<TypedResult<Object>> equivalents = new HashSet<>();
		equivalents.add(puppies);
		equivalents.add(puppies2);
		equivalents.add(puppies3);

		assertEquals(1, equivalents.size());
	}

	@Test
	public void testUntypedNonFailure() {
		assertThrows(IllegalArgumentException.class, () -> TypedResult.fail(ActionResult.ok()));

	}

	@Test
	public void testFilterWithStandardErrorMessage() {
		TypedResult<String> shouldPass = TypedResult.ok("Foo").peek(Assertions::assertNotNull);
		assertTrue(shouldPass.isOk());
		TypedResult<String> shouldStillpass = shouldPass.filter("Foo"::equals, "Foo is not itself");
		assertTrue(shouldStillpass.isOk());
		TypedResult<String> shouldNotPass = shouldStillpass.filter("Bar"::equals, "Foo is not bar");
		assertFalse(shouldNotPass.isOk());
		shouldNotPass.peek(Assertions::assertNull);
		assertEquals("Foo is not bar", shouldNotPass.getMessage());
		TypedResult<String> shouldStillNotPass = shouldNotPass.filter("Baz"::equals, "Bar is not baz");
		assertEquals("Foo is not bar", shouldStillNotPass.getMessage());
	}

	@Test
	public void testFilterWithProducedErrorMessage() {
		TypedResult<String> shouldPass = TypedResult.ok("Foo");
		assertTrue(shouldPass.isOk());
		TypedResult<String> shouldStillpass = shouldPass.filter("Foo"::equals, in -> in + " is not Foo");
		assertTrue(shouldStillpass.isOk());
		TypedResult<String> shouldNotPass = shouldStillpass.filter("Bar"::equals, in -> in + " is not Bar");
		assertFalse(shouldNotPass.isOk());
		assertEquals("Foo is not Bar", shouldNotPass.getMessage());
		TypedResult<String> shouldStillNotPass = shouldNotPass.filter("Baz"::equals, in -> in + " is not Baz");
		assertEquals("Foo is not Bar", shouldStillNotPass.getMessage());
	}


	@Test
	public void testEqualsAndHashCode() {
		TypedResult<String> a = TypedResult.ok("A");
		TypedResult<String> b = TypedResult.fail("A");
		TypedResult<String> c = TypedResult.ok(null);


		assertFalse(a.equals(b));
		assertFalse(b.equals(a));
		assertTrue(a.equals(a));
		assertFalse(a.equals(null));
		assertFalse(a.equals("A"));
		assertFalse(a.equals(c));
		assertFalse(c.equals(a));
		assertFalse(c.equals(b));
		assertFalse(b.equals(c));


		assertEquals(a.hashCode(), a.hashCode());
		assertNotEquals(a.hashCode(), b.hashCode());
		assertEquals(b.hashCode(), b.hashCode());
	}

	@Test
	public void testActionResultAttempts() {
		ActionResult shouldSucceed = ActionResult.attempt(() -> {
		});
		assertTrue(shouldSucceed.isOk());

		ActionResult shouldFail = ActionResult.attempt(() -> {
			throw new Exception("Test");
		});
		assertFalse(shouldFail.isOk());
		assertEquals(shouldFail.getMessage(), "Test");
	}

	@Test
	public void testRecordFields() {
		ActionResult no = ActionResult.error("Not good");
		assertThat(no, instanceOf(Failure.class));
		Failure f = (Failure) no;
		assertEquals(f.message(), no.getMessage());

		TypedResult<String> alsoNo = TypedResult.fail("Also not good");
		assertThat(alsoNo, instanceOf(TypedFailure.class));
		TypedFailure<String> tf = (TypedFailure<String>) alsoNo;
		assertEquals(tf.message(), alsoNo.getMessage());

		TypedResult<String> yes = TypedResult.ok("Good");
		assertThat(yes, instanceOf(TypedSuccess.class));
		TypedSuccess<String> ts = (TypedSuccess<String>) yes;
		assertEquals(ts.object(), yes.getObject());

		reflexive(f);
		reflexive(tf);
		reflexive(ts);
	}

	@Test
	public void testOptionalConversion() {
		assertTrue(TypedResult.ok("OK").asOptional().isPresent());
		assertTrue(TypedResult.fail("Not OK").asOptional().isEmpty());
	}
}
